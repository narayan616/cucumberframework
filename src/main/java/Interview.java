import com.sun.codemodel.internal.JMethod;
import io.cucumber.messages.internal.com.google.gson.internal.bind.util.ISO8601Utils;

import java.sql.SQLOutput;
import java.util.*;
import java.util.stream.Collectors;

public class Interview {

    public static void main(String[] args) {
        Interview interview = new Interview();
        // divisible();
        //  interview.reverseString();
        //countChar();
        // compareString();
       // distinctChar();
       // toFindWhiteSpace();
      // toFindUpperCase();
       // toFindSpecialCharacter();
       // upperToLowerCase();
       // toDisplayLengthOfString();
       // sortingArray();
      //  isPresent();
        occurence();
    }

    public static void divisible() {
        for (int i = 1; i < 100; i++) {
            if (i % 3 == 0 && i % 5 == 0 && i % 15 == 0) {
                List<Integer> list = new ArrayList<>();
                list.add(i);
                Iterator itr = list.iterator();
                while (itr.hasNext()) {
                    System.out.println("the number which divided by 3, 5 and 15 is  " + itr.next());
                }
                ;
            }
        }
    }

    //Write a program that takes the string input and reverse the string and display it.
    public void reverseString() {
        String str = "narayan sapkota";
        String reverseStr = "";
        for (int i = 0; i < str.length(); i++) {
            reverseStr = str.charAt(i) + reverseStr;
        }
        System.out.println(reverseStr);
    }
    // COUNT CHARACTER OF STRING

    public static void countChar() {
        String string = "narayan sapkota";
        int counter = 0;
        for (int i = 0; i < string.length(); i++) {
            if (string.charAt(i) == 'n') {
                counter++;
            }
        }
        System.out.println('n' + "=" + counter);

    }
// Write a program to take two string as input and check whether they are equal or not (Try
//both equals and ignore case)

    public static void compareString() {
        String input = "Narayan";
        String input_2 = "narayan";
        if (input.equalsIgnoreCase(input_2)) {
            System.out.println("this is equal");
        } else {
            System.out.println("this is not equal");
        }
    }
//Write a program that takes a string input and find distinct characters in string and keep it in
//array. Eg : apple The array should contain ‘aple’ only and print it.

    public static void distinctChar() {
        String input = "apple";
        LinkedHashSet<Character> mySet = new LinkedHashSet<>();
        for (int i = 0; i < input.length(); i++) {
            mySet.add(input.charAt(i));
        }
    //    mySet.stream().collect(Collectors.toList());
        System.out.println(mySet);
        }


//Write a program to find the number of white spaces used in a string. (Suraj maharjan is my name)
    public static void toFindWhiteSpace(){
        String input= "narayan  sapkota";
        int count=0;
        for(int i= 0; i<input.length(); i++){
            if(input.charAt(i)==' '){
                count++;
            }
        }
        System.out.println("number of space in given string is =" +count);
    }

    //Write a program to find the number of uppercase and lower case characters in a string.
    public static void toFindUpperCase(){
        String input="NArayan";
        int Upper=0;
        int lower=0;
        for(int i=0; i<input.length(); i++){
        char ch=input.charAt(i);
        if(ch>='A' && ch<='Z'){
            Upper++;
        }else if(ch>='a' && ch<='z'){
            lower++;
        }
        }
        System.out.println(Upper);
        System.out.println(lower);
    }

//Write a program to find the special characters in the string. Special characters includes !@#$
//
//%^&*()”:>? sur$$$tttt@#$%

    public static void toFindSpecialCharacter(){
        String input="%^&*()”:>? sur$$$tttt@#$%";
        String specialCharacter="";
        for(int i=0; i<input.length(); i++){
            char ch=input.charAt(i);
            if(ch=='$'){
                specialCharacter= specialCharacter+input.charAt(i);
            }
        }
        System.out.println(specialCharacter);

    }

    //Write a program to take string input and convert it to uppercase and lowercase with out
    //using the String function. [ USE ASCII Concept ]
    public static void upperToLowerCase(){
        String input = "narayan sapkoTA";
        System.out.println(input.toUpperCase());
        System.out.println(input.toLowerCase());

    }

    //Write a program to take an array of string and display the length of strings in an array.

    public static void toDisplayLengthOfString(){
        String[] input={"narayan", "sapkot", "hari"};
        for(int i=0; i<input.length; i++){
             ArrayList<Integer> newArry= new ArrayList();
            newArry.add(input[i].length());
            System.out.print(newArry);
        }
        }

        //Write the program to take an array of string input and sort the
        // array according to length of string both ascending and descending.
    public static void sortingArray(){
        String[] input={"narayan", "sapkot", "hari", "rayan"};
        Arrays.sort(input);
        System.out.println(Arrays.toString(input));
        Arrays.sort(input,Collections.reverseOrder());
        System.out.println(Arrays.toString(input));




        Arrays.sort(input, (str1, str2)  ->str1.length() - str2.length());
        Arrays.asList(input).forEach(System.out::rintln);


    }

    //Write a program to check if the word 'orange' is present in the "This is orange juice".
    public static void isPresent(){
        String str_1="orange";
        String str_2="This is the orange juice";

        if(str_2.contains(str_1)){
            System.out.println(str_1 +"is present on " +str_2);
        }
    }

    //Write a program to find the first and the last occurence of the letter 'o' and character ',' in "Hello, World”.

    public static void occurence(){
        String str = "Hello, World";
        for(int i=0; i<str.length(); i++){
            if(str.charAt(i)=='o'){
                System.out.println("occurence of 'o' is " +i);
            }
        }

    }
    }


