package restAssured;

import static io.restassured.RestAssured.*;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.ValidatableResponse;

import static org.hamcrest.Matchers.*;
import static org.testng.Assert.assertEquals;

import org.testng.annotations.Test;

public class SampleTest {
    @Test

    void myTest() {

        RestAssured.baseURI= "https://reqres.in/";
        String response= given().log().all().header("Content-Type","application/json")
                .body(Payload.addPlace())
                .when().post("\n" +
                        "/api/users")
                .then().assertThat().statusCode(201).body("name", equalTo("morpheus")).extract().response().asString();


        System.out.println(response);
        JsonPath js= new JsonPath(response);
        String id= js.get("id");
        System.out.println("id is " +id);
        String createdTime= js.getString("createdAt");
        System.out.println(" created time is " + createdTime);

    }
    @Test
    void myTest1() {
        RestAssured.baseURI= "https://reqres.in/";
        String response1=  given().log().all().header("Content-Type","application/json")
                .body(Payload.updateJob())
                .when().put("/api/users/2")
                .then().assertThat().statusCode(200).body("job", equalTo("zion resident")).extract().response().asString();
        System.out.println("updated response is " + response1);

        JsonPath js1= new JsonPath(response1);
        String job = js1.getString("job");
        System.out.println(job);
        assertEquals(job, "zion resident");
    }

    @Test
    void myTest3() {
        RestAssured.baseURI= "https://reqres.in/";
        String getResponse=	 given().log().all().queryParam("id", "65")
                .when().get("/api/users/2")
                .then().assertThat().statusCode(200).extract().response().asString();
        System.out.println(getResponse);
    }

    @Test
    void myTest4() {
        RestAssured.baseURI= "https://reqres.in/";
        String getResponse=	 given().log().all().queryParam("page", "2")
                .when().get("/api/users")
                .then().log().all().assertThat().statusCode(200).extract().response().asString();
        System.out.println(getResponse);
    }

    @Test
    void myTest5() {
        RestAssured.baseURI= "https://reqres.in/";
        given().log().all().header("Content-Type","application/json").body(Payload.addPlace())
                .when().delete("/api/users/2")
                .then().log().all().assertThat().statusCode(204);

    }
}


