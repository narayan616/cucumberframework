package cucumber.runner;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(features = "/Users/narayansapkota/IdeaProjects/CucumberFramework/src/test/java/cucumber/feature"
,glue = {"cucumber.stepdefinitions"}
,monochrome = false
,plugin = {"pretty", "html:test-output", "json:json_output/cucumber.json", "junit:junit_xml/cucumber.xml"}
,dryRun = false
,publish = true)
public class TestRunner {
}
