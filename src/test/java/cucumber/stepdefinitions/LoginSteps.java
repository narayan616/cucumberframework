package cucumber.stepdefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import cucumber.reusuable.BaseClass;

import java.io.IOException;
import java.util.Iterator;
import java.util.Set;

public class LoginSteps extends BaseClass {

    @Given("Open the Chrome and launch the application")
    public void open_the_chrome_and_launch_the_application() throws IOException {
        System.out.println("browser is open");
        openUrl();
        implicitlyWait();
        screenshot();

    }
    @When("Enter the Username and Password")
    public void enter_the_username_and_password() throws IOException {
        System.out.println("user enable to enter id and password ");
        driver.manage().window().maximize();
        implicitlyWait();
        driver.findElement(By.xpath("//*[@id='nav-link-accountList']")).click();
        implicitlyWait();
         WebElement emailField= driver.findElement(By.xpath("//*[@id='ap_email']"));
          emailField.sendKeys("narayan@ymail.com");
        driver.findElement(By.xpath("//*[@id='continue']")).click();
        driver.findElement(By.xpath("//input[@type='password']")).sendKeys("Aeiou616");
        driver.findElement(By.xpath("//input[@id='signInSubmit']")).click();
        screenshot();

    }
    @Then("Reset the credential")
    public void reset_the_credential() throws IOException {
        System.out.println("user enable to reset credential");
        driver.findElement(By.xpath("//*[@id='auth-fpp-link-bottom']")).click();
        driver.findElement(By.xpath("//*[@id='continue']")).click();
        screenshot();
        driver.quit();
    }
    @When("^Enter the \"([^\"]*)\" and \"([^\"]*)\"$")
    public void enter_the_Username_and_Password(String userName,String password) throws IOException {
        System.out.println("user enable to enter id and password ");
        driver.manage().window().maximize();
        implicitlyWait();
        driver.findElement(By.xpath("//*[@id='nav-link-accountList']")).click();
        implicitlyWait();
        WebElement emailField= driver.findElement(By.xpath("//*[@id='ap_email']"));
        emailField.sendKeys(userName);
        driver.findElement(By.xpath("//*[@id='continue']")).click();
        driver.findElement(By.xpath("//input[@type='password']")).sendKeys(password);
        driver.findElement(By.xpath("//input[@id='signInSubmit']")).click();
        screenshot();



        /////////////////////////
//        String mainWindow=driver.getWindowHandle();
//        Set<String> childwindows=driver.getWindowHandles();
//        Iterator<String> it= childwindows.iterator();
//        while(it.hasNext()){
//         String cw=   it.next();
//            driver.switchTo().window(String.valueOf(cw.indexOf(3)));
        }
        }



