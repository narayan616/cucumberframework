Feature: Reset functionality on login page of Application

  Scenario: Verification of Reset button
    Given Open the Chrome and launch the application
    When Enter the Username and Password
    Then Reset the credential




  Scenario Outline: Verification of login button
    Given Open the Chrome and launch the application
    When Enter the "<Username>" and "<password>"
    Then Reset the credential
    Examples:
      |Username                   | password |
      |  narayan@yopmail.com      |   Aeiou616       |
      |  ryan@yopmail.com      |   Asdf1234    |