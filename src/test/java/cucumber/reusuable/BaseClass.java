package cucumber.reusuable;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

public class BaseClass {
   public WebDriver driver;

    public void openUrl() {
        System.setProperty("webdriver.chrome.driver","/Users/narayansapkota/IdeaProjects/" +
                "CucumberFramework/src/test/Driver/chromedriver 2" );
        driver= new ChromeDriver();
        driver.get("https://www.amazon.com");
        driver.manage().window().maximize();
    }

    public  void screenshot() throws IOException {
        File screenshot= ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(screenshot, new File("/Users/narayansapkota/IdeaProjects/CucumberFramework/src/test/Screenshot/testScreen"
        +Timestamp()+"screenshot.png"));

    }

    private static String Timestamp() {
        return new SimpleDateFormat("yyyy-MM-dd hh-mm-ss").format(new Date());
    }

    public  void implicitlyWait(){
        driver.manage().timeouts().implicitlyWait(6, TimeUnit.SECONDS);
    }

    public static <V> void waitWithCondition(WebDriverWait wait, String type, By by) {
        switch (type) {
            case "elementClickable":
                wait.until(ExpectedConditions.elementToBeClickable(by));
                break;
            case "presenceOfElement":
                wait.until((Function<? super WebDriver, V>) ExpectedConditions.presenceOfElementLocated(by));
                break;

            case "visibilityOfElement":
                wait.until((Function<? super WebDriver, V>) ExpectedConditions.visibilityOfElementLocated(by));
                break;
        }
    }


}
